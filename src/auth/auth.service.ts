import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { PersonsService } from './../persons/persons.service';

@Injectable()
export class AuthService {
  constructor(
    private personsService: PersonsService,
    private jwtService: JwtService,
  ) {}

  async validateUser(userid: string, pass: string): Promise<any> {
    const person = await this.personsService.findOne(userid);
    const user = {
      userId: person.passport,
      username: userid,
    };
    if (person && person.passport === pass) {
      const { ...result } = user;
      return result;
    }
    return null;
  }

  async login(user: any) {
    const payload = { username: user.username, sub: user.userId };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
