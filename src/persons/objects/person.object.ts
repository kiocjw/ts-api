import { ID, Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class PersonType {
  @Field(() => ID)
  id: string;
  @Field()
  readonly firstName: string;
  @Field()
  readonly lastName: string;
  @Field()
  readonly passport: string;
  @Field({ nullable: true })
  readonly email?: string;
  @Field({ nullable: true })
  readonly address?: string;
  @Field({ nullable: true })
  username?: string;
}
