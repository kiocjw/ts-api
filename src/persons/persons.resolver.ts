import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { UnauthorizedException, UseGuards } from '@nestjs/common';
import { PersonsService } from './persons.service';
import { PersonType } from './objects/person.object';
import { PersonInput } from './inputs/person.input';
import { decrypt, encrypt } from './../encryption';
import { CurrentUser } from './../auth/current-user.decorator';
import { User } from './objects/user.object';
import { GqlAuthGuard } from './../auth/guards/graphql.guard';

export const HOSTNAME =
  'https://ts-api-xvdmxzkhuq-ue.a.run.app';

@Resolver()
export class PersonsResolver {
  constructor(private readonly personsService: PersonsService) {}

  @Query(() => String)
  async index() {
    return 'Index';
  }

  /*
  @Query(returns => String)
  @UseGuards(GqlAuthGuard)
  whoAmI(@CurrentUser() user: User) {
    return user.userId;
  }s

  @Mutation(() => PersonType)
  @UseGuards(GqlAuthGuard)
  async createPerson(@Args('input') input: PersonInput) {
    return this.personsService.create(input);
  }

  @Query(() => [PersonType])
  async persons() {
    return this.personsService.findAll();
  }
  */

  @Mutation(() => String)
  async createPersonURL(@Args('input') input: PersonInput) {
    const person = await this.personsService.findByPassport(input.passport);
    if (person) {
      throw new Error('Passport is already exist!');
    }
    return (
      encodeURI(`${HOSTNAME}/graphql?query={getPerson(code:"`) +
      `${encrypt((await this.personsService.createurl(input)).toString())}` +
      encodeURI(`"){id,firstName,lastName,passport}}`)
    );
  }

  @Query(() => PersonType)
  async getPerson(@Args('code') code: string) {
    try {
      const person = await this.personsService.findByID(decrypt(code));
      return person;
    } catch (error) {
      return {
        id: 'null',
        firstName: 'null',
        lastName: 'null',
        passport: 'null',
      };
    }
  }

  @Query(() => String)
  @UseGuards(GqlAuthGuard)
  async getPersonURLWithToken(
    @CurrentUser() user: User,
    @Args('input') input: PersonInput,
  ) {
    if (!user) {
      throw new UnauthorizedException();
    }
    const jwtPerson = await this.personsService.findByPassport(user.id);
    if (
      jwtPerson.firstName === input.firstName &&
      jwtPerson.lastName === input.lastName &&
      jwtPerson.passport === input.passport
    ) {
      return (
        encodeURI(`${HOSTNAME}/graphql?query={getPersonWithToken(code:"`) +
        `${encrypt((await this.personsService.createurl(input)).toString())}` +
        encodeURI(`"){id,firstName,lastName,passport}}`)
      );
    } else {
      return 'Invalid owner info!';
    }
  }

  @Query(() => PersonType)
  @UseGuards(GqlAuthGuard)
  async getPersonWithToken(
    @CurrentUser() user: User,
    @Args('code') code: string,
  ) {
    if (!user) {
      throw new UnauthorizedException();
    }
    const person = await this.personsService.findByID(decrypt(code));
    const jwtPerson = await this.personsService.findByPassport(user.id);

    if (
      person &&
      person.firstName === jwtPerson.firstName &&
      person.lastName === jwtPerson.lastName &&
      person.passport === jwtPerson.passport
    ) {
      return person;
    } else {
      throw new Error('Unmatch owner info!');
    }
  }
}
