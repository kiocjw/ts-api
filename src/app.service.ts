import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getIndex(): string {
    return 'Welcome to TS-API! <br> <a href="graphql">GraphQL</a>';
  }
}
